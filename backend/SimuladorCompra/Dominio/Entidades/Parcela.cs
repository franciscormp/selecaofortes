﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.Entidades
{
    public class Parcela : EntidadeBase
    {
        public decimal Valor { get; set; }
        public decimal Juros { get; set; }
        public DateTime Vencimento { get; set; }
        public decimal Montante { get; set; }
        public int SimulacaoId { get; set; }
        public virtual Simulacao Simulacao { get; set; }
    }
}
