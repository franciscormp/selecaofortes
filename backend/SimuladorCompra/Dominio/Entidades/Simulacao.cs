﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.Entidades
{
    public class Simulacao : EntidadeBase
    {
        public Simulacao()
        {
            Parcelas = new List<Parcela>();
        }
     
        public int QuantidadeParcelas { get; set; }

        public decimal Juros { get; set; }

        public decimal ValorCompra { get; set; }

        public decimal TotalFinal { get; private set; }

        public decimal? ValorParcela { get; set; }

        public DateTime DataCompra { get; set; }

        public virtual List<Parcela> Parcelas { get; set; }

        public void ValorTotal()
        {
            var parcela = 0m;
           
            parcela = (Juros / 100) * ValorCompra;
            ValorParcela = parcela + (ValorCompra / QuantidadeParcelas);

            TotalFinal = ValorCompra + (parcela * QuantidadeParcelas);
            
        }
    }
}
