﻿using Dominio.Entidades;
using Dominio.Interfaces.Repositorios;
using Dominio.Interfaces.Servicos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.Servicos
{
    public class SimulacaoService : ServiceBase<Simulacao>, ISimulacaoService
    {
        private readonly ISimulacaoRepository _simulacaoRepository;

        public SimulacaoService(ISimulacaoRepository simulacaoRepository)
            : base(simulacaoRepository)
        {
            _simulacaoRepository = simulacaoRepository;
        }
    }
}
