﻿using Dominio.Entidades;
using Dominio.Interfaces.Repositorios;
using Dominio.Interfaces.Servicos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.Servicos
{
    public class ParcelaService : ServiceBase<Parcela>, IParcelaService
    {
        private readonly IParcelaRepository _parcelaRepository;

        public ParcelaService(IParcelaRepository parcelaRepository)
            :base(parcelaRepository)
        {
            _parcelaRepository = parcelaRepository;
        }
    }
}
