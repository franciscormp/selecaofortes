﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Dominio.Interfaces.Repositorios
{
   public interface IRepositoryBase<TEntity> where TEntity : class
    {
        void Add(TEntity obj);
        TEntity GetById(int id);
        IEnumerable<TEntity> GetAll();
       // IEnumerable<TEntity> GetAllWithChild();
        void Update(TEntity obj);
        void Remove(TEntity obj);
        void Dispose();
    }
}
