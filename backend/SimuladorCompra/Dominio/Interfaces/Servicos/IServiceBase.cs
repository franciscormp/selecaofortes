﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.Interfaces.Servicos
{
    public interface IServiceBase<TEntity> where TEntity : class
    {
        void Add(TEntity obj);
        TEntity GetById(int id);
        IEnumerable<TEntity> GetAll();
        //IEnumerable<TEntity> GetAllWithChild();
        void Update(TEntity obj);
        void Remove(TEntity obj);
        void Dispose();
    }
}
