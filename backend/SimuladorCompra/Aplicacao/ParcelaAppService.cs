﻿using Aplicacao.Interfaces;
using Dominio.Entidades;
using Dominio.Interfaces.Servicos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aplicacao
{
   public class ParcelaAppService : AppServiceBase<Parcela>, IParcelaAppService
    {
        private readonly IParcelaService _parcelaService;

        public ParcelaAppService(IParcelaService parcelaService)
            :base(parcelaService)
        {
            _parcelaService = parcelaService;
        }
    }
}
