﻿using Aplicacao.Interfaces;
using Dominio.Entidades;
using Dominio.Interfaces.Servicos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aplicacao
{
    public class SimulacaoAppService : AppServiceBase<Simulacao>, ISimulacaoAppService
    {

        private readonly ISimulacaoService _simulacaoService;

        public SimulacaoAppService(ISimulacaoService simulacaoService)
            : base(simulacaoService)
        {
            _simulacaoService = simulacaoService;
        }
       
    }
}
