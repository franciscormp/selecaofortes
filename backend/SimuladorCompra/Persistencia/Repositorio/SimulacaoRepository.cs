﻿using Dominio.Entidades;
using Dominio.Interfaces.Repositorios;
using Microsoft.EntityFrameworkCore;
using Persistencia.Contexto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistencia.Repositorio
{
    public class SimulacaoRepository : RepositoryBase<Simulacao>, ISimulacaoRepository
    {
        public SimulacaoRepository(ApplicationDbContext dbContext)
            : base(dbContext)
        {
        }    
        
        public IEnumerable<Simulacao> GetAll()
        {
            return base.Db.Simuacoes.Include(s => s.Parcelas);                
        }
       
    }
}
