﻿using Dominio.Entidades;
using Dominio.Interfaces.Repositorios;
using Persistencia.Contexto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistencia.Repositorio
{
    public class ParcelaRepository : RepositoryBase<Parcela>, IParcelaRepository
    {
        public ParcelaRepository(ApplicationDbContext dbContext)
            :base(dbContext)
        {
                
        }
       
    }
}
