﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistencia.Migrations
{
    public partial class Inicial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Simuacoes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    QuantidadeParcelas = table.Column<int>(nullable: false),
                    Juros = table.Column<decimal>(nullable: false),
                    ValorCompra = table.Column<decimal>(nullable: false),
                    TotalFinal = table.Column<decimal>(nullable: false)                    
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Simuacoes", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Simuacoes");
        }
    }
}
