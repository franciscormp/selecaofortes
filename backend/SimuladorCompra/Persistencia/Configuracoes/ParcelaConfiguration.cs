﻿using Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistencia.Configuracoes
{
    public class ParcelaConfiguration : IEntityTypeConfiguration<Parcela>
    {
        public void Configure(EntityTypeBuilder<Parcela> builder)
        {            
            builder.ToTable("Parcela");
            builder.HasKey(s => s.Id).HasName("id");
            builder.Property(s => s.Id).HasColumnName("id").ValueGeneratedOnAdd();

             builder.HasOne(p => p.Simulacao)
            .WithMany(b => b.Parcelas);

        }
    }
}
