﻿using Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistencia.Configuracoes
{
    public class SimulacaoConfiguration : IEntityTypeConfiguration<Simulacao>
    {
        public void Configure(EntityTypeBuilder<Simulacao> builder)
        {           
            builder.ToTable("Simulacao");
            builder.HasKey(s => s.Id).HasName("id");
            builder.Property(s => s.Id).HasColumnName("id").ValueGeneratedOnAdd();
        }
    }
}
