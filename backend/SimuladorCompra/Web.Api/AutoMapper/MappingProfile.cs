﻿using AutoMapper;
using Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Api.AutoMapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<DTOs.SimulacaoDTO, Simulacao>();
            CreateMap<Simulacao, DTOs.SimulacaoDTO>();

            CreateMap<DTOs.ParcelaDTO, Parcela>();
            CreateMap<Parcela, DTOs.ParcelaDTO>();
        }
    }
}
