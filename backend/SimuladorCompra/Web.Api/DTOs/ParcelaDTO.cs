﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Api.DTOs
{
    public class ParcelaDTO
    {
        public int Id { get; set; }

        public decimal Valor { get; set; }

        public decimal Juros { get; set; }

        public DateTime Vencimento { get; set; }

        public decimal Montante { get; set; }

        public int SimulacaoId { get; set; }
                
    }
}
