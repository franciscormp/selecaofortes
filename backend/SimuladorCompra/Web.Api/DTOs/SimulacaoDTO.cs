﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Api.DTOs
{
    public class SimulacaoDTO
    {
        public int Id { get; set; }

        public int QuantidadeParcelas { get; set; }

        public decimal Juros { get; set; }

        public decimal ValorCompra { get; set; }

        public decimal TotalFinal { get; set; }

        public decimal? ValorParcela { get; set; }

        public DateTime DataCompra { get; set; }

        public virtual List<ParcelaDTO> Parcelas { get; set; }
    }
}
