﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Aplicacao.Interfaces;
using AutoMapper;
using Dominio.Entidades;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Web.Api.DTOs;

namespace Web.Api.Controllers
{
    
    [Route("api/v1/[controller]")]
    [ApiController]
    public class SimulacaoController : ControllerBase
    {
        private readonly ISimulacaoAppService _simulacaoApp;
        private readonly IParcelaAppService _parcelaApp;
        private readonly IMapper _mapper;

        public SimulacaoController(ISimulacaoAppService simualcaoApp,
                                   IParcelaAppService parcelaApp,
                                   IMapper mapper)
        {
            _simulacaoApp = simualcaoApp;
            _parcelaApp = parcelaApp;
            _mapper = mapper;
        }

        // GET: api/v1/Simulacao
        [HttpGet]
        public IEnumerable<SimulacaoDTO> Get()
        {
           
            return _mapper.Map<List<SimulacaoDTO>>(_simulacaoApp.GetAll().ToList());
           
        }

        // GET: api/v1/Simulacao/5
        [HttpGet("{id}")]
        public ActionResult<SimulacaoDTO> Get(int id)
        {       

            return _mapper.Map<SimulacaoDTO>(_simulacaoApp.GetAll()
                                                          .ToList()
                                                          .Where(s => s.Id == id)
                                                          .FirstOrDefault());          
        }

        // GET: api/v1/Simulacao/GetParcela/5
        [HttpGet]
        [Route("GetParcela/{idParcela}")]
        public ActionResult<ParcelaDTO> GetParcela(int idParcela)
        {
            return _mapper.Map<ParcelaDTO>(_parcelaApp.GetById(idParcela));
        }


        // POST: api/v1/Simulacao
        [HttpPost]
        public void Post([FromBody] SimulacaoDTO simulacao)
        {
            var simulacaoEntity = _mapper.Map<Simulacao>(simulacao);
             simulacaoEntity.ValorTotal();
             //simulacaoEntity.DataCompra = DateTime.Now;
            try
            {
               _simulacaoApp.Add(simulacaoEntity);
                var simulacaoId = simulacaoEntity.Id;
                var contador = 1;
                for (int i = 0; i < simulacaoEntity.QuantidadeParcelas; i++)
                {
                    var parcela = new Parcela();

                    parcela.Juros = simulacaoEntity.Juros;
                    parcela.Montante = simulacaoEntity.TotalFinal;
                    parcela.Valor = (decimal)simulacaoEntity.ValorParcela;
                    parcela.Vencimento = simulacaoEntity.DataCompra.AddMonths(contador);
                    parcela.SimulacaoId = simulacaoId;

                    _parcelaApp.Add(parcela);

                    contador ++;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    
    }
}
